package com.example.oauthcompose.presentation.screens.login

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.example.oauthcompose.R
import com.example.oauthcompose.ui.theme.topAppBarBackgroundColor
import com.example.oauthcompose.ui.theme.topAppBarContentColor

@Composable
fun LoginTopBarCompose() {
    TopAppBar(
        title = {
            Text(
                text = stringResource(R.string.login_topbar_title),
                color = MaterialTheme.colors.topAppBarContentColor
            )
        },
        backgroundColor = MaterialTheme.colors.topAppBarBackgroundColor
    )
}