package com.example.oauthcompose.presentation.screens.profile

import androidx.compose.foundation.background
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import com.example.oauthcompose.R
import com.example.oauthcompose.components.DisplayAlertDialogCompose
import com.example.oauthcompose.ui.theme.topAppBarBackgroundColor
import com.example.oauthcompose.ui.theme.topAppBarContentColor

@Composable
fun ProfileTopBarCompose(
    onSave: () -> Unit,
    onDeleteAllConfirmed: () -> Unit
) {
    TopAppBar(
        title = {
            Text(
                text = stringResource(R.string.profile_topbar_title),
                color = MaterialTheme.colors.topAppBarContentColor
            )
        },
        backgroundColor = MaterialTheme.colors.topAppBarBackgroundColor,
        actions = {
            ProfileTopBarActions(onSave = onSave, onDeleteAllConfirmed = onDeleteAllConfirmed)
        }
    )
}

@Composable
private fun ProfileTopBarActions(
    onSave: () -> Unit,
    onDeleteAllConfirmed: () -> Unit
) {
    var openDialog by remember { mutableStateOf(false) }

    DisplayAlertDialogCompose(
        openDialog = openDialog,
        onYesClicked = onDeleteAllConfirmed,
        onDialogClosed = { openDialog = false }
    )

    SaveAction(onSave = onSave)
    DeleteAllAction(onDelete = { openDialog = true })
}

@Composable
private fun SaveAction(onSave: () -> Unit) {
    IconButton(onClick = onSave) {
        Icon(
            painter = painterResource(R.drawable.ic_save),
            contentDescription = null,
            tint = MaterialTheme.colors.topAppBarContentColor
        )
    }
}

@Composable
private fun DeleteAllAction(onDelete: () -> Unit) {
    var expanded by remember { mutableStateOf(false) }

    IconButton(onClick = { expanded = true }) {
        Icon(
            painter = painterResource(R.drawable.ic_vertical_menu),
            contentDescription = null,
            tint = MaterialTheme.colors.topAppBarContentColor
        )

        DropdownMenu(
            modifier = Modifier.background(color = Color.Red),
            expanded = expanded,
            onDismissRequest = { expanded = false }
        ) {
            DropdownMenuItem(
                onClick = {
                    expanded = false
                    onDelete()
                }) {
                Text(
                    text = stringResource(R.string.delete_account_text),
                    color = MaterialTheme.colors.topAppBarContentColor
                )
            }
        }
    }
}

/*@Preview
@Composable
fun ProfilePreview() {
    ProfileTopBarCompose(onSave = {}, onDeleteAllConfirmed = {})
}*/
