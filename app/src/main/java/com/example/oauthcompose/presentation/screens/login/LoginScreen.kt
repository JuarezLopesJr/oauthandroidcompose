package com.example.oauthcompose.presentation.screens.login

import android.app.Activity
import android.util.Log
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.platform.LocalContext
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.oauthcompose.domain.model.ApiRequest
import com.example.oauthcompose.domain.model.ApiResponse
import com.example.oauthcompose.utils.RequestState
import com.example.oauthcompose.utils.StartActivityForResultCompose
import com.example.oauthcompose.utils.navigateToProfileScreen
import com.example.oauthcompose.utils.signIn
import com.example.oauthcompose.viewmodel.LoginViewModel
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator

@Destination(start = true)
@Composable
fun LoginScreenCompose(
    navigator: DestinationsNavigator,
    loginViewModel: LoginViewModel = hiltViewModel()
) {
    val signedInState by loginViewModel.signedInState
    val messageBarState by loginViewModel.messageBarState
    val apiResponse by loginViewModel.apiResponse
    val activity = LocalContext.current as Activity

    Scaffold(
        topBar = { LoginTopBarCompose() },
        content = {
            LoginContentCompose(
                signedInState = signedInState,
                messageBarState = messageBarState,
                onButtonClicked = {
                    loginViewModel.saveSignedInStateViewModel(signedInState = true)
                }
            )
        }
    )

    StartActivityForResultCompose(
        key = signedInState,
        onResultReceived = { tokenId ->
            Log.d("Login", tokenId)
            loginViewModel.verifyTokenOnBackend(request = ApiRequest(tokenId))
        },
        onDialogDismissed = {
            loginViewModel.saveSignedInStateViewModel(signedInState = false)
        }
    ) { activityLauncher ->
        if (signedInState) {
            signIn(
                activity = activity,
                launchActivityResult = { intentSenderRequest ->
                    activityLauncher.launch(intentSenderRequest)
                },
                accountNotFound = {
                    loginViewModel.saveSignedInStateViewModel(signedInState = false)
                    loginViewModel.updateMessageBarState()
                }
            )
        }
    }

    LaunchedEffect(key1 = apiResponse) {
        when (apiResponse) {
            is RequestState.Success -> {
                val response =
                    (apiResponse as RequestState.Success<ApiResponse>).data.success

                if (response) {
                    navigateToProfileScreen(navigator = navigator)
                } else {
                    loginViewModel.saveSignedInStateViewModel(signedInState = false)
                }
            }
            else -> {}
        }
    }
}