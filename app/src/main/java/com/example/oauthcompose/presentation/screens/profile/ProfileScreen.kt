package com.example.oauthcompose.presentation.screens.profile

import android.app.Activity
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.ui.platform.LocalContext
import androidx.hilt.navigation.compose.hiltViewModel
import coil.annotation.ExperimentalCoilApi
import com.example.oauthcompose.domain.model.ApiRequest
import com.example.oauthcompose.domain.model.ApiResponse
import com.example.oauthcompose.utils.RequestState
import com.example.oauthcompose.utils.StartActivityForResultCompose
import com.example.oauthcompose.utils.navigateToLoginScreen
import com.example.oauthcompose.utils.signIn
import com.example.oauthcompose.viewmodel.ProfileViewModel
import com.google.android.gms.auth.api.identity.Identity
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import retrofit2.HttpException

@ExperimentalCoilApi
@Destination
@Composable
fun ProfileScreenCompose(
    navigator: DestinationsNavigator,
    profileViewModel: ProfileViewModel = hiltViewModel()
) {
    val apiResponse by profileViewModel.apiResponse
    val messageBarState by profileViewModel.messageBarState
    val user by profileViewModel.user
    val firstName by profileViewModel.firstName
    val lastName by profileViewModel.lastName
    val clearSessionResponse by profileViewModel.clearSessionResponse
    val activity = LocalContext.current as Activity

    Scaffold(
        topBar = {
            ProfileTopBarCompose(
                onSave = {
                    profileViewModel.updateUserInfo()
                },
                onDeleteAllConfirmed = {
                    profileViewModel.deleteUser()
                }
            )
        },
        content = {
            ProfileContentCompose(
                apiResponse = apiResponse,
                messageBarState = messageBarState,
                firstName = firstName,
                onFirstNameChanged = {
                    profileViewModel.updateFirstName(newName = it)
                },
                lastName = lastName,
                onLastNameChanged = {
                    profileViewModel.updateLastName(newName = it)
                },
                emailAddress = user?.emailAddress ?: "johndoe@gmail.com",
                profileImage = user?.profileImage,
                onSignOutClicked = {
                    profileViewModel.clearSession()
                }
            )
        }
    )

    StartActivityForResultCompose(
        key = apiResponse,
        onResultReceived = {
            profileViewModel.verifyTokenOnBackend(request = ApiRequest(tokenId = it))
        },
        onDialogDismissed = {
            profileViewModel.saveSignedInState(signedIn = true)
            navigateToLoginScreen(navigator = navigator)
        }
    ) { activityLauncher ->
        if (apiResponse is RequestState.Success) {
            val response = (apiResponse as RequestState.Success<ApiResponse>).data
            if (response.error is HttpException && response.error.code() == 401) {
                signIn(
                    activity = activity,
                    accountNotFound = {
                        profileViewModel.saveSignedInState(signedIn = false)
                        navigateToLoginScreen(navigator = navigator)
                    },
                    launchActivityResult = {
                        activityLauncher.launch(it)
                    }
                )
            }
        } else if (apiResponse is RequestState.Error) {
            profileViewModel.saveSignedInState(signedIn = false)
            navigateToLoginScreen(navigator = navigator)
        }
    }

    LaunchedEffect(key1 = clearSessionResponse) {
        if (clearSessionResponse is RequestState.Success &&
            (clearSessionResponse as RequestState.Success<ApiResponse>).data.success
        ) {
            val oneTapClient = Identity.getSignInClient(activity)
            oneTapClient.signOut()
            profileViewModel.saveSignedInState(signedIn = false)
            navigateToLoginScreen(navigator = navigator)
        }
    }
}