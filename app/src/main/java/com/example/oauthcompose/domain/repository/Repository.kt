package com.example.oauthcompose.domain.repository

import com.example.oauthcompose.domain.model.ApiRequest
import com.example.oauthcompose.domain.model.ApiResponse
import com.example.oauthcompose.domain.model.UserUpdate
import kotlinx.coroutines.flow.Flow

interface Repository {
    suspend fun saveSignedInStateDomainRepo(signedIn: Boolean)

    fun readSignedInStateDomainRepo(): Flow<Boolean>

    suspend fun verifyTokenOnBackendDomainRepo(request: ApiRequest): ApiResponse

    suspend fun getUserInfoDomainRepo(): ApiResponse

    suspend fun updateUserInfoDomainRepo(userUpdate: UserUpdate): ApiResponse

    suspend fun deleteUserDomainRepo(): ApiResponse

    suspend fun clearSessionDomainRepo(): ApiResponse
}