package com.example.oauthcompose.domain.model

import kotlinx.serialization.Serializable

@Serializable
data class UserUpdate(
    val firstName: String? = null,
    val lastName: String? = null,
)
