package com.example.oauthcompose.viewmodel

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.oauthcompose.domain.model.ApiRequest
import com.example.oauthcompose.domain.model.ApiResponse
import com.example.oauthcompose.domain.model.MessageBarState
import com.example.oauthcompose.domain.repository.Repository
import com.example.oauthcompose.utils.GoogleAccountNotFoundException
import com.example.oauthcompose.utils.RequestState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val repository: Repository
) : ViewModel() {

    private val _signedInState: MutableState<Boolean> = mutableStateOf(false)

    val signedInState: State<Boolean>
        get() = _signedInState

    private val _messageBarState: MutableState<MessageBarState> = mutableStateOf(MessageBarState())

    val messageBarState: State<MessageBarState>
        get() = _messageBarState

    private val _apiResponse: MutableState<RequestState<ApiResponse>> =
        mutableStateOf(RequestState.Idle)

    val apiResponse: State<RequestState<ApiResponse>>
        get() = _apiResponse

    init {
        viewModelScope.launch {
            repository.readSignedInStateDomainRepo().collect { state ->
                _signedInState.value = state
            }
        }
    }

    fun saveSignedInStateViewModel(signedInState: Boolean) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.saveSignedInStateDomainRepo(signedInState)
        }
    }

    fun updateMessageBarState() {
        _messageBarState.value = MessageBarState(error = GoogleAccountNotFoundException())
    }

    fun verifyTokenOnBackend(request: ApiRequest) {
        _apiResponse.value = RequestState.Loading

        try {
            viewModelScope.launch(Dispatchers.IO) {
                val response = repository.verifyTokenOnBackendDomainRepo(request)
                _apiResponse.value = RequestState.Success(data = response)
                _messageBarState.value = MessageBarState(
                    message = response.message,
                    error = response.error
                )
            }
        } catch (e: Exception) {
            _apiResponse.value = RequestState.Error(e)
            _messageBarState.value = MessageBarState(error = e)
        }
    }

    fun getUserInfo() {
        _apiResponse.value = RequestState.Loading

        try {
            viewModelScope.launch(Dispatchers.IO) {
                val response = repository.getUserInfoDomainRepo()
                _apiResponse.value = RequestState.Success(data = response)
                _messageBarState.value = MessageBarState(
                    message = response.message,
                    error = response.error
                )
            }
        } catch (e: Exception) {
            _apiResponse.value = RequestState.Error(e)
            _messageBarState.value = MessageBarState(error = e)
        }
    }

}