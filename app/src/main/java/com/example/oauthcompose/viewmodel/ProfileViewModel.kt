package com.example.oauthcompose.viewmodel

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.oauthcompose.domain.model.ApiRequest
import com.example.oauthcompose.domain.model.ApiResponse
import com.example.oauthcompose.domain.model.MessageBarState
import com.example.oauthcompose.domain.model.User
import com.example.oauthcompose.domain.model.UserUpdate
import com.example.oauthcompose.domain.repository.Repository
import com.example.oauthcompose.utils.EmptyFieldException
import com.example.oauthcompose.utils.NothingToUpdateException
import com.example.oauthcompose.utils.RequestState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val repository: Repository
) : ViewModel() {

    private val _user: MutableState<User?> = mutableStateOf(null)

    val user: State<User?>
        get() = _user


    private val _firstName: MutableState<String> = mutableStateOf("")

    val firstName: State<String>
        get() = _firstName

    private val _lastName: MutableState<String> = mutableStateOf("")

    val lastName: State<String>
        get() = _lastName

    private val _apiResponse: MutableState<RequestState<ApiResponse>> =
        mutableStateOf(RequestState.Idle)

    val apiResponse: State<RequestState<ApiResponse>>
        get() = _apiResponse

    private val _messageBarState: MutableState<MessageBarState> =
        mutableStateOf(MessageBarState())

    val messageBarState: State<MessageBarState>
        get() = _messageBarState

    private val _clearSessionResponse: MutableState<RequestState<ApiResponse>> =
        mutableStateOf(RequestState.Idle)

    val clearSessionResponse: State<RequestState<ApiResponse>> = _clearSessionResponse

    init {
        getUserInfo()
    }

    private fun getUserInfo() {
        viewModelScope.launch {
            _apiResponse.value = RequestState.Loading

            try {
                val response = withContext(Dispatchers.IO) {
                    repository.getUserInfoDomainRepo()
                }
                _apiResponse.value = RequestState.Success(data = response)

                _messageBarState.value = MessageBarState(
                    message = response.message,
                    error = response.error
                )

                response.user?.let {
                    _user.value = response.user
                    _firstName.value = response.user.name.split(" ").first()
                    _lastName.value = response.user.name.split(" ").last()
                }
            } catch (e: Exception) {
                _apiResponse.value = RequestState.Error(e)
                _messageBarState.value = MessageBarState(error = e)
            }
        }
    }

    private fun verifyAndUpdate(currentUser: ApiResponse) {
        val (verified, exception) =
            if (firstName.value.isEmpty() || lastName.value.isEmpty()) {
                Pair(false, EmptyFieldException())
            } else {
                if (currentUser.user?.name?.split(" ")?.first() == firstName.value &&
                    currentUser.user.name.split(" ").last() == lastName.value
                ) {
                    Pair(false, NothingToUpdateException())
                } else {
                    Pair(true, null)
                }
            }

        viewModelScope.launch(Dispatchers.IO) {
            if (verified) {
                try {
                    val response = repository.updateUserInfoDomainRepo(
                        userUpdate = UserUpdate(
                            firstName = firstName.value,
                            lastName = lastName.value
                        )
                    )
                    _apiResponse.value = RequestState.Success(response)
                    _messageBarState.value = MessageBarState(
                        message = response.message,
                        error = response.error
                    )
                } catch (e: Exception) {
                    _apiResponse.value = RequestState.Error(e)
                    _messageBarState.value = MessageBarState(error = e)
                }
            } else {
                _apiResponse.value = RequestState.Success(
                    ApiResponse(success = false, error = exception)
                )
                _messageBarState.value = MessageBarState(error = exception)
            }
        }
    }

    fun updateUserInfo() {
        viewModelScope.launch(Dispatchers.IO) {
            _apiResponse.value = RequestState.Loading

            try {
                user.value?.let {
                    val response = repository.getUserInfoDomainRepo()
                    verifyAndUpdate(currentUser = response)
                }
            } catch (e: Exception) {
                _apiResponse.value = RequestState.Error(e)
                _messageBarState.value = MessageBarState(error = e)
            }
        }
    }

    fun clearSession() {
        _clearSessionResponse.value = RequestState.Loading
        _apiResponse.value = RequestState.Loading
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.clearSessionDomainRepo()
                _clearSessionResponse.value = RequestState.Success(response)
                _apiResponse.value = RequestState.Success(response)
                _messageBarState.value = MessageBarState(
                    message = response.message,
                    error = response.error
                )
            } catch (e: Exception) {
                _clearSessionResponse.value = RequestState.Error(e)
                _apiResponse.value = RequestState.Error(e)
                _messageBarState.value = MessageBarState(error = e)
            }
        }
    }

    fun deleteUser() {
        _clearSessionResponse.value = RequestState.Loading
        _apiResponse.value = RequestState.Loading
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val response = repository.deleteUserDomainRepo()
                _clearSessionResponse.value = RequestState.Success(response)
                _apiResponse.value = RequestState.Success(response)
                _messageBarState.value = MessageBarState(
                    message = response.message,
                    error = response.error
                )
            } catch (e: Exception) {
                _clearSessionResponse.value = RequestState.Error(e)
                _apiResponse.value = RequestState.Error(e)
                _messageBarState.value = MessageBarState(error = e)
            }
        }
    }

    fun verifyTokenOnBackend(request: ApiRequest) {
        _apiResponse.value = RequestState.Loading
        try {
            viewModelScope.launch(Dispatchers.IO) {
                val response = repository.verifyTokenOnBackendDomainRepo(request = request)
                _apiResponse.value = RequestState.Success(response)
                _messageBarState.value = MessageBarState(
                    message = response.message,
                    error = response.error
                )
            }
        } catch (e: Exception) {
            _apiResponse.value = RequestState.Error(e)
            _messageBarState.value = MessageBarState(error = e)
        }
    }

    fun saveSignedInState(signedIn: Boolean) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.saveSignedInStateDomainRepo(signedIn = signedIn)
        }
    }

    fun updateFirstName(newName: String) {
        if (newName.length < 20) _firstName.value = newName
    }

    fun updateLastName(newName: String) {
        if (newName.length < 20) _lastName.value = newName
    }
}