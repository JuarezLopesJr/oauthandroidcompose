@file:OptIn(ExperimentalCoilApi::class)

package com.example.oauthcompose.utils

import android.app.Activity
import android.util.Log
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.ActivityResult
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.platform.LocalContext
import coil.annotation.ExperimentalCoilApi
import com.example.oauthcompose.presentation.screens.destinations.LoginScreenComposeDestination
import com.example.oauthcompose.presentation.screens.destinations.ProfileScreenComposeDestination
import com.example.oauthcompose.utils.Constants.CLIENT_ID
import com.example.oauthcompose.utils.Constants.SIGN_IN_TAG
import com.example.oauthcompose.utils.Constants.START_ACTIVITY_TAG
import com.google.android.gms.auth.api.identity.BeginSignInRequest
import com.google.android.gms.auth.api.identity.Identity
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.CommonStatusCodes
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import com.ramcosta.composedestinations.navigation.popUpTo

@Composable
fun StartActivityForResultCompose(
    key: Any,
    onResultReceived: (String) -> Unit,
    onDialogDismissed: () -> Unit,
    launcher: (ManagedActivityResultLauncher<IntentSenderRequest, ActivityResult>) -> Unit
) {
    val activity = LocalContext.current as Activity

    val activityLauncher =
        rememberLauncherForActivityResult(
            contract = ActivityResultContracts.StartIntentSenderForResult()
        ) { activityResult ->
            try {
                if (activityResult.resultCode == Activity.RESULT_OK) {
                    val oneTapClient = Identity.getSignInClient(activity)

                    val credentials =
                        oneTapClient.getSignInCredentialFromIntent(activityResult.data)

                    val tokenId = credentials.googleIdToken

                    tokenId?.let {
                        onResultReceived(tokenId)
                    }
                } else {
                    Log.d(START_ACTIVITY_TAG, "How did you get here ????")
                    onDialogDismissed()
                }
            } catch (e: ApiException) {
                when (e.statusCode) {
                    CommonStatusCodes.CANCELED -> {
                        Log.d(START_ACTIVITY_TAG, "tap dialog canceled")
                        onDialogDismissed()
                    }
                    CommonStatusCodes.NETWORK_ERROR -> {
                        Log.d(START_ACTIVITY_TAG, "network error")
                        onDialogDismissed()
                    }
                    else -> {
                        Log.d(START_ACTIVITY_TAG, "${e.message}")
                        onDialogDismissed()
                    }
                }
            }
        }

    LaunchedEffect(key1 = key) {
        launcher(activityLauncher)
    }
}

fun signIn(
    activity: Activity,
    launchActivityResult: (IntentSenderRequest) -> Unit,
    accountNotFound: () -> Unit
) {
    val oneTapClient = Identity.getSignInClient(activity)

    val signInRequest = BeginSignInRequest.builder()
        .setGoogleIdTokenRequestOptions(
            BeginSignInRequest.GoogleIdTokenRequestOptions.builder()
                .setSupported(true)
                .setServerClientId(CLIENT_ID)
                .setFilterByAuthorizedAccounts(true)
                .build()
        )
        .setAutoSelectEnabled(true)
        .build()

    oneTapClient.beginSignIn(signInRequest)
        .addOnSuccessListener { result ->
            try {
                launchActivityResult(
                    IntentSenderRequest.Builder(
                        result.pendingIntent.intentSender
                    ).build()
                )
            } catch (e: Exception) {
                Log.d(SIGN_IN_TAG, "One Tap error: ${e.message}")
            }
        }
        .addOnFailureListener {
            Log.d(SIGN_IN_TAG, "Account not found inside smartphone exception")

            signUp(
                activity = activity,
                launchActivityResult = launchActivityResult,
                accountNotFound = accountNotFound
            )
        }
}

fun signUp(
    activity: Activity,
    launchActivityResult: (IntentSenderRequest) -> Unit,
    accountNotFound: () -> Unit
) {
    val oneTapClient = Identity.getSignInClient(activity)

    val signInRequest = BeginSignInRequest.builder()
        .setGoogleIdTokenRequestOptions(
            BeginSignInRequest.GoogleIdTokenRequestOptions.builder()
                .setSupported(true)
                .setServerClientId(CLIENT_ID)
                .setFilterByAuthorizedAccounts(false)
                .build()
        )
        .build()

    oneTapClient.beginSignIn(signInRequest)
        .addOnSuccessListener { result ->
            try {
                launchActivityResult(
                    IntentSenderRequest.Builder(
                        result.pendingIntent.intentSender
                    ).build()
                )
            } catch (e: Exception) {
                Log.d(SIGN_IN_TAG, "One Tap error: ${e.message}")
            }
        }
        .addOnFailureListener {
            Log.d(SIGN_IN_TAG, "Account not found inside smartphone exception")
            accountNotFound()
        }
}

class GoogleAccountNotFoundException(
    override val message: String? = Constants.ACCOUNT_EXCEPTION
) : Exception()

class NothingToUpdateException(
    override val message: String = "Nothing to update"
) : Exception()

class EmptyFieldException(
    override val message: String = "Empty Input Field."
) : Exception()

fun navigateToLoginScreen(navigator: DestinationsNavigator) {
    navigator.navigate(LoginScreenComposeDestination) {
        popUpTo(ProfileScreenComposeDestination) {
            inclusive = true
        }
    }
}

fun navigateToProfileScreen(navigator: DestinationsNavigator) {
    navigator.navigate(ProfileScreenComposeDestination) {
        popUpTo(LoginScreenComposeDestination) {
            inclusive = true
        }
    }
}