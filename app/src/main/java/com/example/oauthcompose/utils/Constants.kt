package com.example.oauthcompose.utils

object Constants {
    // Network
//    const val BASE_URL = "http://10.0.2.2:8080"
    const val BASE_URL = "https://oauthktor.herokuapp.com"
    const val TIME_OUT_VALUE = 15L
    const val MEDIA_TYPE_JSON = "application/json"
    const val CONNECTION_TIMEOUT_EXCEPTION = "Connection Timeout Exception"
    const val CONNECTION_EXCEPTION = "Internet Connection Unavailable"

    // DataStore
    const val USER_PREFERENCES = "user_preferences"
    const val PREFERENCES_SIGNED_IN_KEY = "signed_in_key"

    // Google SignIn
    const val CLIENT_ID = "645986613321-9rf3ljli4ciatapdtc06smnl6g1cio3m.apps.googleusercontent.com"
    const val SIGN_IN_TAG = "sign_in_debug_tag"
    const val START_ACTIVITY_TAG = "StartActivityIntent"
    const val ACCOUNT_EXCEPTION = "Google Account Not Found"
}