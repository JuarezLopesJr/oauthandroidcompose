package com.example.oauthcompose.data.remote

import com.example.oauthcompose.domain.model.ApiRequest
import com.example.oauthcompose.domain.model.ApiResponse
import com.example.oauthcompose.domain.model.UserUpdate
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT

interface KtorApi {

    @POST("/token_verification")
    suspend fun verifyTokenOnBackendApi(@Body request: ApiRequest): ApiResponse

    @GET("/get_user")
    suspend fun getUserInfoApi(): ApiResponse

    @PUT("/update_user")
    suspend fun updateUserInfoApi(@Body userUpdate: UserUpdate): ApiResponse

    @DELETE("/delete_user")
    suspend fun deleteUserApi(): ApiResponse

    @GET("/sign_out")
    suspend fun clearSessionApi(): ApiResponse
}