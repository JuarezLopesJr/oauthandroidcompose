package com.example.oauthcompose.data.repository

import com.example.oauthcompose.data.remote.KtorApi
import com.example.oauthcompose.domain.model.ApiRequest
import com.example.oauthcompose.domain.model.ApiResponse
import com.example.oauthcompose.domain.model.UserUpdate
import com.example.oauthcompose.domain.repository.DataStoreOperations
import com.example.oauthcompose.domain.repository.Repository
import javax.inject.Inject
import kotlinx.coroutines.flow.Flow

class RepositoryImpl @Inject constructor(
    private val dataStoreOperations: DataStoreOperations,
    private val ktorApi: KtorApi
) : Repository {
    override suspend fun saveSignedInStateDomainRepo(signedIn: Boolean) {
        dataStoreOperations.saveSignedInState(signedIn)
    }

    override suspend fun verifyTokenOnBackendDomainRepo(request: ApiRequest): ApiResponse {
        return try {
            ktorApi.verifyTokenOnBackendApi(request)
        } catch (e: Exception) {
            ApiResponse(success = false, error = e)
        }
    }

    override suspend fun getUserInfoDomainRepo(): ApiResponse {
        return try {
            ktorApi.getUserInfoApi()
        } catch (e: Exception) {
            ApiResponse(success = false, error = e)
        }
    }

    override suspend fun updateUserInfoDomainRepo(userUpdate: UserUpdate): ApiResponse {
        return try {
            ktorApi.updateUserInfoApi(userUpdate)
        } catch (e: Exception) {
            ApiResponse(success = false, error = e)
        }
    }

    override suspend fun deleteUserDomainRepo(): ApiResponse {
        return try {
            ktorApi.deleteUserApi()
        } catch (e: Exception) {
            ApiResponse(success = false, error = e)
        }
    }

    override suspend fun clearSessionDomainRepo(): ApiResponse {
        return try {
            ktorApi.clearSessionApi()
        } catch (e: Exception) {
            ApiResponse(success = false, error = e)
        }
    }

    override fun readSignedInStateDomainRepo(): Flow<Boolean> {
        return dataStoreOperations.readSignedInState()
    }
}