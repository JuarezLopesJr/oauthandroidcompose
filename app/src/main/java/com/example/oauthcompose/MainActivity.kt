package com.example.oauthcompose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import coil.annotation.ExperimentalCoilApi
import com.example.oauthcompose.presentation.screens.NavGraphs
import com.example.oauthcompose.ui.theme.OAuthComposeTheme
import com.ramcosta.composedestinations.DestinationsNavHost
import dagger.hilt.android.AndroidEntryPoint

@ExperimentalCoilApi
@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            OAuthComposeTheme {
                DestinationsNavHost(navGraph = NavGraphs.root)
            }
        }
    }
}