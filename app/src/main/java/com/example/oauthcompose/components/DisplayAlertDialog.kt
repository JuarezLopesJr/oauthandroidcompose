package com.example.oauthcompose.components

import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import com.example.oauthcompose.R

@Composable
fun DisplayAlertDialogCompose(
    title: String = stringResource(R.string.confirm_delete_dialog_title),
    message: String = stringResource(R.string.confirm_delete_dialog_message),
    openDialog: Boolean,
    onYesClicked: () -> Unit,
    onDialogClosed: () -> Unit,
) {
    if (openDialog) {
        AlertDialog(
            title = {
                Text(
                    text = title,
                    fontSize = MaterialTheme.typography.h5.fontSize,
                    fontWeight = FontWeight.Bold
                )
            },
            text = {
                Text(
                    text = message,
                    fontSize = MaterialTheme.typography.subtitle1.fontSize,
                    fontWeight = FontWeight.Normal
                )
            },
            confirmButton = {
                Button(
                    onClick = {
                        onYesClicked()
                        onDialogClosed()
                    }
                ) {
                    Text(text = stringResource(R.string.confirm_button_text))
                }
            },
            dismissButton = {
                OutlinedButton(
                    onClick = {
                        onDialogClosed()
                    }
                ) {
                    Text(text = stringResource(R.string.cancel_button_text))
                }
            },
            onDismissRequest = onDialogClosed
        )
    }
}